// Code JavaScript
const difficultySelect = document.getElementById('difficulty-select');
const textToType = document.getElementById('text-to-type');
const typingArea = document.getElementById('typing-area');
const checkButton = document.getElementById('check-button');
const results = document.getElementById('results');

let startTime;

// Tableau de phrases prédéfinies
const phrases = {
  easy: [
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
    "Curabitur in lacus non turpis vehicula dictum.",
    "Aenean non mollis sem, at vestibulum velit."
  ],
  medium: [
    "Praesent in ligula eu magna placerat viverra.",
    "Proin a mauris non dui fermentum aliquam.",
    "Nunc auctor, tellus non aliquet aliquam, ipsum diam ornare est."
  ],
  hard: [
    "Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.",
    "Vivamus at dui a diam aliquet eleifend.",
    "Suspendisse in nisi quis nisi viverra fermentum."
  ]
};

// Sélection aléatoire d'une phrase en fonction de la difficulté sélectionnée
const selectRandomPhrase = () => {
    const difficulty = difficultySelect.value;
    textToType.textContent = phrases[difficulty][Math.floor(Math.random() * phrases[difficulty].length)];
  };
  
  selectRandomPhrase();
  
  difficultySelect.addEventListener('change', selectRandomPhrase);
  
  checkButton.addEventListener('click', () => {
    if (typingArea.value === textToType.textContent) {
      const endTime = new Date();
      const timeElapsed = (endTime - startTime) / 1000; // temps en secondes
      let score;
  
      // Calcul du score en fonction du temps écoulé et de la difficulté sélectionnée
      if (difficultySelect.value === 'easy') {
        if (timeElapsed < 10) {
          score = 5;
        } else if (timeElapsed < 20) {
          score = 4;
        } else if (timeElapsed < 30) {
          score = 3;
        } else if (timeElapsed < 40) {
          score = 2;
        } else {
          score = 1;
        }
      } else if (difficultySelect.value === 'medium') {
        if (timeElapsed < 20) {
          score = 5;
        } else if (timeElapsed < 30) {
          score = 4;
        } else if (timeElapsed < 40) {
          score = 3;
        } else if (timeElapsed < 50) {
          score = 2;
        } else {
          score = 1;
        }
      } else if (difficultySelect.value === 'hard') {
        if (timeElapsed < 30) {
          score = 5;
        } else if (timeElapsed < 40) {
          score = 4;
        } else if (timeElapsed < 50) {
          score = 3;
        } else if (timeElapsed < 60) {
          score = 2;
        } else {
          score = 1;
        }
      }
  
      results.textContent = `Vous avez terminé en ${timeElapsed} secondes et votre score est de ${score}/5.`;
    } else {
      results.textContent = 'Le texte tapé ne correspond pas au texte à taper.';
    }
  });
  
  typingArea.addEventListener('focus', () => {
    startTime = new Date();
  });
 
  